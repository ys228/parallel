#include "elevation.h"

std::vector<std::pair<int, int>> DIRECTIONS = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};

std::vector<std::vector<Point>> read_elevation(std::string filename, int N)
{
    std::vector<std::vector<Point>> point_matrix(N, std::vector<Point>(N, Point()));
    std::ifstream input_stream;
    input_stream.open(filename);

    if (input_stream.is_open())
    {
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++) {
                int elevation = 0;
                input_stream >> elevation;

                point_matrix[i][j] = Point(i, j, elevation);
            }
    }
    input_stream.close();

    return point_matrix;
}

void check_elevation(std::vector<std::vector<Point>> &point_matrix, int row, int col)
{
    int N = point_matrix.size();
    float max_diff = 0;

    std::vector<std::pair<int, std::pair<int, int>>> candidates;

    for (std::pair<int, int> dir : DIRECTIONS)
    {
        int roww = row + dir.first;
        int coll = col + dir.second;

        if (roww >= 0 && roww < N && coll >= 0 && coll < N)
        {
            float diff = point_matrix[row][col].altitude - point_matrix[roww][coll].altitude;
            if (diff > 0)
            {
                candidates.push_back(std::make_pair(diff, std::make_pair(roww, coll)));
                max_diff = std::max(max_diff, diff);
            }
        }
    }

    for (std::pair<int, std::pair<int, int>> candidate : candidates)
    {
        if (candidate.first == max_diff) {
            point_matrix[row][col].candidates.push_back(candidate.second);
        }
    }
}
