#ifndef _ELEVATION__H
#define _ELEVATION__H

#include <iostream>
#include <fstream>
#include <vector>
#include <utility> // std::pair, std::make_pair
#include <string>
#include "point.h"


std::vector<std::vector<Point>> read_elevation(std::string filename, int N);

void check_elevation(std::vector<std::vector<Point>> &point_matrix, int row, int col);

#endif
