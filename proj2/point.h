#ifndef _POINT__H
#define _POINT__H

#include <iostream>
#include <fstream>
#include <vector>
#include <utility>
#include <string>

#include "math.h"

class Point
{
public:
    int x = 0;
    int y = 0;
    int altitude = 0;

    float raindrop = 0;
    float absorbtion = 0;

    float trickle_val = 0;
    std::vector<std::pair<int, int>> candidates;

    // constructor.
    Point() {}
    Point(int x, int y, int altitude) : x(x), y(y), altitude(altitude) {}

    // fall a drop of rain.
    void drop() { raindrop++; }

    // absorb the amount of rain.
    void absorb(float A)
    {
        float temp = raindrop;
        raindrop = std::max((float)0, temp - A);
        absorbtion += temp - raindrop;
    }

    // trickle the water to its candidates and then clear the candidates.
    void trickle_cleanup()
    {
        trickle_val = 0;
    }
};

#endif
