#include "rain_fall.h"

void raindrop_and_absorbtion(std::vector<std::vector<Point>> &point_matrix, float &A, int &current, int &M)
{
    int row = point_matrix.size();

    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < row; j++)
        {
            if (current < M)
            {
                point_matrix[i][j].drop();
            }
            point_matrix[i][j].absorb(A);
        }
    }
}

void prepare_trickle(std::vector<std::vector<Point>> &point_matrix)
{
    int row = point_matrix.size();

    for (int i = 0; i < row; i++)
        for (int j = 0; j < row; j++)
            check_elevation(point_matrix, i, j);
}

void trickle(std::vector<std::vector<Point>> &point_matrix)
{
    int row = point_matrix.size();

    for (int i = 0; i < row; i++)
        for (int j = 0; j < row; j++)
            point_matrix[i][j].trickle_val = point_matrix[i][j].raindrop;

    for (int i = 0; i < row; i++)
        for (int j = 0; j < row; j++)
        {
            for (std::pair<int, int> p : point_matrix[i][j].candidates)
            {
                float original_value = fmin(1, point_matrix[i][j].trickle_val);
                float delta = original_value / point_matrix[i][j].candidates.size();

                point_matrix[i][j].raindrop -= delta;
                point_matrix[p.first][p.second].raindrop += delta;
            }

            point_matrix[i][j].trickle_cleanup();
        }
}
