#include "rain_fall_pt.h"
#include "simulation_pt.h"

void raindrop_and_absorbtion(std::vector<std::vector<Point>> &point_matrix, float &A, int &current, int &M, int thread_idx, int P, pthread_barrier_t &barrier)
{
    int N = point_matrix.size();
    int row_start = thread_idx * (int)ceil(N / P);
    int row_end = std::min(N, (thread_idx + 1) * (int)ceil(N / P));

    for (int i = row_start; i < row_end; i++)
    {
        for (int j = 0; j < N; j++)
        {
            if (current < M)
            {
                point_matrix[i][j].drop();
            }
            point_matrix[i][j].absorb(A);
        }
    }
}

void prepare_trickle(std::vector<std::vector<Point>> &point_matrix)
{
    int row = point_matrix.size();

    for (int i = 0; i < row; i++)
        for (int j = 0; j < row; j++)
            check_elevation(point_matrix, i, j);
}

void trickle(std::vector<std::vector<Point>> &point_matrix, pthread_mutex_t *mutex_array, int thread_idx, int P, pthread_barrier_t &barrier)
{
    int N = point_matrix.size();
    int row_start = thread_idx * (int)ceil(N / P);
    int row_end = std::min(N, (thread_idx + 1) * (int)ceil(N / P));

    for (int i = row_start; i < row_end; i++)
        for (int j = 0; j < N; j++)
            point_matrix[i][j].trickle_val = point_matrix[i][j].raindrop;

    pthread_barrier_wait(&barrier);

    for (int i = row_start; i < row_end; i++)
        for (int j = 0; j < N; j++)
        {
            for (std::pair<int, int> p : point_matrix[i][j].candidates)
            {
                float original_value = fmin(1, point_matrix[i][j].trickle_val);
                float delta = original_value / point_matrix[i][j].candidates.size();

                point_matrix[i][j].raindrop -= delta;

                if (p.first == row_start - 1 || p.first == row_end)
                    pthread_mutex_lock(&mutex_array[p.first * N + p.second]);

                point_matrix[p.first][p.second].raindrop += delta;

                if (p.first == row_start - 1 || p.first == row_end)
                    pthread_mutex_unlock(&mutex_array[p.first * N + p.second]);
            }

            point_matrix[i][j].trickle_cleanup();
        }
}
