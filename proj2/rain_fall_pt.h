#ifndef _RAINFALL_PT__H
#define _RAINFALL_PT__H

#include <vector>
#include <utility> // std::pair, std::make_pair
#include <algorithm>
#include <string>
#include <cmath>

#include "math.h"
#include "elevation_pt.h"
#include "point.h"

void raindrop_and_absorbtion(std::vector<std::vector<Point>> &point_matrix, float &A, int &current, int &M, int thread_idx, int P, pthread_barrier_t &barrier);

void prepare_trickle(std::vector<std::vector<Point>> &point_matrix);

void trickle(std::vector<std::vector<Point>> &point_matrix, pthread_mutex_t *mutex_array, int thread_idx, int P, pthread_barrier_t &barrier);

#endif
