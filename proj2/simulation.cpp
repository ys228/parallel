#include <omp.h>
#include "simulation.h"

void step_forward(std::vector<std::vector<Point>> &point_matrix, int &current, int &M, float &A)
{
    // 1. a drop of rain may fall onto a point in the landscape and absorb.
    raindrop_and_absorbtion(point_matrix, A, current, M);

    // 2. if there remains any rain drops on a point at a given timestep, some amount will trickle away to the neighboring point(s) with the lowest elevation.
    trickle(point_matrix);
}

bool check_empty(std::vector<std::vector<Point>> &point_matrix)
{
    int row = point_matrix.size();

    for (int i = 0; i < row; i++)
        for (int j = 0; j < row; j++)
            if (point_matrix[i][j].raindrop != 0)
                return false;

    return true;
}

int main(int argc, char *argv[])
{
    // read from argv for paramters.
    int P = atoi(argv[1]);
    int M = atoi(argv[2]);
    float A = atof(argv[3]);
    int N = atoi(argv[4]);
    std::string elevation_file = argv[5];

    // read from elevation file.
    std::vector<std::vector<Point>> point_matrix = read_elevation(elevation_file, N);

    // prepare timing.
    double t_start, t_end;
    t_start = omp_get_wtime();

    // start simluation.
    prepare_trickle(point_matrix);

    int current = 0;
    while (current == 0 || check_empty(point_matrix) == false)
    {
        step_forward(point_matrix, current, M, A);
        current++;
    }

    // end timing.
    t_end = omp_get_wtime();

    // output the result.
    std::cout << "Rainfall simulation completed in " << current << " time steps" << std::endl;
    std::cout << "Runtime = " << t_end - t_start << " seconds\n"
              << std::endl;
    std::cout << "The following grid shows the number of raindrops absorbed at each point:" << std::endl;

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            std::cout << "\t" << point_matrix[i][j].absorbtion;
        }
        std::cout << std::endl;
    }

    return 0;
}
