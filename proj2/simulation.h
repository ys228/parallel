#ifndef _SIMULATION__H
#define _SIMULATION__H

#include "rain_fall.h"
#include "elevation.h"
#include "point.h"
#include <pthread.h>


void step_forward(std::vector<std::vector<Point>> &point_matrix, int &current, int &M, float &A);

bool check_empty(std::vector<std::vector<Point>> &raindrop_matrix);

#endif
