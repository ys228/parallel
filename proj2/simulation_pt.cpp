#include <omp.h>
#include "simulation_pt.h"

int current = 0;
pthread_barrier_t barrier;

void step_forward(std::vector<std::vector<Point>> &point_matrix, int &M, float &A, int &P, int thread_idx, pthread_mutex_t *mutex_array)
{
    // 1. a drop of rain may fall onto a point in the landscape.
    raindrop_and_absorbtion(point_matrix, A, current, M, thread_idx, P, barrier);

    pthread_barrier_wait(&barrier);

    // 2. if there remains any rain drops on a point at a given timestep, some amount will trickle away to the neighboring point(s) with the lowest elevation.
    trickle(point_matrix, mutex_array, thread_idx, P, barrier);

    if (thread_idx == 0)
        current++;

    pthread_barrier_wait(&barrier);
}

void *simulate(void *args)
{
    struct args targs = *((struct args *)args);

    while (current == 0 || check_empty(*targs.point_matrix) == false)
    {
        pthread_barrier_wait(&barrier);

        step_forward(*targs.point_matrix, targs.M, targs.A, targs.P, targs.thread_idx, targs.mutex_array);
    }
    free(args);
}

bool check_empty(std::vector<std::vector<Point>> &point_matrix)
{
    int row = point_matrix.size();

    for (int i = 0; i < row; i++)
        for (int j = 0; j < row; j++)
            if (point_matrix[i][j].raindrop != 0)
                return false;

    return true;
}

int main(int argc, char *argv[])
{
    // read from argv for paramters.
    if (argc != 6)
    {
        std::cout << "usage: ./rainfall <P> <M> <A> <N> <elevation_file>" << std::endl;
        exit(EXIT_FAILURE);
    }

    int P = atoi(argv[1]);
    int M = atoi(argv[2]);
    float A = atof(argv[3]);
    int N = atoi(argv[4]);
    std::string elevation_file = argv[5];

    // read from elevation file.
    std::vector<std::vector<Point>> point_matrix = read_elevation(elevation_file, N);

    // init pthread related stuff.
    pthread_mutex_t *mutex_array = new pthread_mutex_t[N * N];
    for (size_t i = 0; i < N * N; i++)
    {
        pthread_mutex_init(&mutex_array[i], NULL);
    }
    pthread_barrier_init(&barrier, NULL, P);

    pthread_t *threads = (pthread_t *)malloc(P * sizeof(pthread_t));

    // prepare timing.
    double t_start, t_end;
    t_start = omp_get_wtime();

    // start simluation.
    prepare_trickle(point_matrix);

    for (int i = 0; i < P; i++)
    {
        struct args *targs = (struct args *)malloc(sizeof(struct args));
        targs->thread_idx = i;
        targs->P = P;
        targs->M = M;
        targs->A = A;
        targs->N = N;
        targs->point_matrix = &point_matrix;
        targs->mutex_array = mutex_array;

        pthread_create(threads + i, nullptr, simulate, (void *)targs);
    }

    for (int i = 0; i < P; ++i)
    {
        pthread_join(threads[i], nullptr);
    }

    // end timing.
    t_end = omp_get_wtime();

    // output the result.
    std::cout << "Rainfall simulation completed in " << current << " time steps" << std::endl;
    std::cout << "Runtime = " << t_end - t_start << " seconds\n"
              << std::endl;
    std::cout << "The following grid shows the number of raindrops absorbed at each point:" << std::endl;

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            std::cout << "\t" << point_matrix[i][j].absorbtion;
        }
        std::cout << std::endl;
    }

    // cleanup the pthread related stuff.
    pthread_barrier_destroy(&barrier);
    for (size_t i = 0; i < N + 2; i++)
    {
        pthread_mutex_destroy(mutex_array + i);
    }

    delete[] mutex_array;

    return 0;
}
