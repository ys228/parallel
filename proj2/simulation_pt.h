#ifndef _SIMULATION_PT__H
#define _SIMULATION_PT__H

#include "rain_fall_pt.h"
#include "elevation_pt.h"
#include "point.h"
#include <pthread.h>

struct args
{
  int thread_idx;
  int P;
  int M;
  float A;
  int N;

  std::vector<std::vector<Point>> *point_matrix;
  pthread_mutex_t *mutex_array;
};

// FUNCTION DECLARE.
void step_forward(std::vector<std::vector<Point>> &point_matrix, int &M, float &A, int &P, int thread_idx, pthread_mutex_t *mutex_array);

void *simulate(void *arg);

bool check_empty(std::vector<std::vector<Point>> &raindrop_matrix);

#endif
